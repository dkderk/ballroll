﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotater : MonoBehaviour
{
    private Vector3 scale;
    private Vector3 bigscale;

    void Awake()
    {
        scale = new Vector3(0.001f, 0.001f, 0.001f);
        bigscale = new Vector3(0.5f, 0.5f, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(15, 30, 45) * Time.deltaTime);
        transform.localScale -= scale;
        if (transform.localScale.y < 0.1f) 
        { 
            transform.localScale += bigscale;
        }
    }
}
